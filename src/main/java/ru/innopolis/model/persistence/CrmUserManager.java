package ru.innopolis.model.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ru.innopolis.model.xjc.CrmUser;

/**
 * Реализация {@link EntityManager} для сущности {@link CrmUser}
 */
public class CrmUserManager extends EntityManager<CrmUser> {

    private static final String SELECT_ALL = "SELECT id AS crm_user_id, login AS crm_user_login, " +
            "user_password AS crm_user_password, first_name AS crm_user_first_name, " +
            "last_name AS crm_user_last_name, email AS crm_user_email, is_active AS crm_user_is_active, " +
            "is_deleted AS crm_user_is_deleted FROM crm_user crm_user";

    private static final String INSERT_CRM_USER = "INSERT INTO crm_user " +
            "(id, login, user_password, first_name, last_name, email, is_active, is_deleted) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String DELETE_CRM_USER_BY_ID = "DELETE FROM crm_user WHERE id=?";

    @Override
    protected void fillInsertStatement(PreparedStatement insertStatement, CrmUser entity) throws SQLException {

        insertStatement.setLong(1, entity.getId());
        insertStatement.setString(2, entity.getLogin());
        insertStatement.setString(3, entity.getPassword());
        insertStatement.setString(4, entity.getFirstName());
        insertStatement.setString(5, entity.getLastName());
        insertStatement.setString(6, entity.getEmail());
        insertStatement.setBoolean(7, entity.isActive());
        insertStatement.setBoolean(8, entity.isDeleted());
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement deleteStatement, CrmUser entity) throws SQLException {
        deleteStatement.setLong(1, entity.getId());
    }

    @Override
    protected String getDeleteByIdStatement() {
        return DELETE_CRM_USER_BY_ID;
    }

    @Override
    protected String getInsertSql() {
        return INSERT_CRM_USER;
    }

    @Override
    protected String getSelectAllQuery() {
        return SELECT_ALL;
    }

    @Override
    protected String getSelectByPkQuery() {
        return SELECT_ALL + " WHERE id=?";
    }

    @Override
    protected CrmUser createNewInstance(ResultSet resultSet) throws SQLException {

        CrmUser crmUser = new CrmUser();
        crmUser.setId(resultSet.getLong("crm_user_id"));
        crmUser.setLogin(resultSet.getString("crm_user_login"));
        crmUser.setEmail(resultSet.getString("crm_user_email"));
        crmUser.setFirstName(resultSet.getString("crm_user_first_name"));
        crmUser.setLastName(resultSet.getString("crm_user_last_name"));
        crmUser.setPassword(resultSet.getString("crm_user_password"));
        crmUser.setActive(resultSet.getBoolean("crm_user_is_active"));
        crmUser.setDeleted(resultSet.getBoolean("crm_user_is_deleted"));
        return crmUser;
    }
}
