package ru.innopolis.model.persistence;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import ru.innopolis.model.xjc.Task;

/**
 * Реализация {@link EntityManager} для сущности {@link Task}
 */
public class TaskManager extends EntityManager<Task> {

    private static final String SELECT_ALL = "SELECT task.id AS task_id, task.title AS task_title, " +
            "task.description AS task_description, task.is_lab AS task_is_lab, task.created AS task_created" +
            " FROM task task";

    private static final String INSERT_TASK = "INSERT INTO task (id, title, description, is_lab, created) " +
            "VALUES (?, ?, ?, ?, ?)";

    private static final String DELETE_TASK_BY_ID = "DELETE FROM task WHERE id=?";

    @Override
    protected void fillInsertStatement(PreparedStatement insertStatement, Task entity) throws SQLException {
        insertStatement.setLong(1, entity.getId());
        insertStatement.setString(2, entity.getTitle());
        insertStatement.setString(3, entity.getDescription());
        insertStatement.setBoolean(4, entity.isLab());
        insertStatement.setDate(5, new Date(entity.getCreated().getTimeInMillis()));
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement deleteStatement, Task entity) throws SQLException {
        deleteStatement.setLong(1, entity.getId());
    }

    @Override
    protected String getDeleteByIdStatement() {
        return DELETE_TASK_BY_ID;
    }


    @Override
    protected String getInsertSql() {
        return INSERT_TASK;
    }

    @Override
    protected String getSelectAllQuery() {
        return SELECT_ALL;
    }

    @Override
    protected String getSelectByPkQuery() {
        return SELECT_ALL + " WHERE id=?";
    }

    @Override
    protected Task createNewInstance(ResultSet resultSet) throws SQLException {
        Task task = new Task();
        task.setId(resultSet.getLong("task_id"));
        task.setTitle(resultSet.getString("task_title"));
        task.setDescription(resultSet.getString("task_description"));
        task.setLab(resultSet.getBoolean("task_is_lab"));
        Calendar taskCalendar = Calendar.getInstance();
        resultSet.getTimestamp("task_created", taskCalendar);
        task.setCreated(taskCalendar);

        return task;
    }
}
