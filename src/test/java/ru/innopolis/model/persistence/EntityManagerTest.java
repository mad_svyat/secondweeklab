package ru.innopolis.model.persistence;

import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Set;

import ru.innopolis.model.xjc.Criterion;
import ru.innopolis.model.xjc.CrmUser;
import ru.innopolis.model.xjc.Mark;
import ru.innopolis.model.xjc.Student;
import ru.innopolis.model.xjc.Task;

import static org.junit.Assert.*;

/**
 *
 */
public class EntityManagerTest {

    private static CrmUser crmUser;
    private static Student student;
    private static Criterion criterion;
    private static Task task;
    private static Mark mark;

    @BeforeClass
    public static void init() {
        crmUser = new CrmUser();
        crmUser.setId(88);
        crmUser.setEmail("user@mail.ru");
        crmUser.setLogin("login");
        crmUser.setFirstName("first");
        crmUser.setLastName("last");
        crmUser.setPassword("#passWord*");
        crmUser.setActive(true);
        crmUser.setDeleted(false);

        student = new Student();
        student.setCrmUser(crmUser);
        student.setDeleted(false);
        student.setId(4233242);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        criterion = new Criterion();
        criterion.setId(4593839);
        criterion.setTitle("test criterion");
        criterion.setMaxPoints(88);
        criterion.setAdditional(false);
        criterion.setCreated(calendar);

        task = new Task();
        task.setId(123433);
        task.setLab(true);
        task.setDescription("Small task description");
        task.setTitle("Laboratory work number 5");
        task.setCreated(calendar);

        mark = new Mark();
        mark.setId(4590774);
        mark.setCriterion(criterion);
        mark.setStudent(student);
        mark.setTask(task);
        mark.setPoints(45);
        mark.setDeleted(false);
    }

    @Test
    public void testCrmUserManager() {
        EntityManager<CrmUser> crmUserManager = new CrmUserManager();
        Set<CrmUser> crmUsers = crmUserManager.getAll();

        final int initialSize = crmUsers.size();

        crmUserManager.insert(crmUser);

        crmUsers = crmUserManager.getAll();
        assertEquals(initialSize + 1, crmUsers.size());

        CrmUser actualCrmUser = crmUsers.stream()
                .filter(u -> u.getId() == crmUser.getId())
                .findFirst().get();

        assertNotNull(actualCrmUser);
        assertEquals(crmUser.isDeleted(), actualCrmUser.isDeleted());
        assertEquals(crmUser.isActive(), actualCrmUser.isActive());
        assertEquals(crmUser.getLogin(), actualCrmUser.getLogin());
        assertEquals(crmUser.getFirstName(), actualCrmUser.getFirstName());
        assertEquals(crmUser.getLastName(), actualCrmUser.getLastName());
        assertEquals(crmUser.getEmail(), actualCrmUser.getEmail());
        assertEquals(crmUser.getPassword(), actualCrmUser.getPassword());

        crmUserManager.delete(actualCrmUser);

        assertEquals(initialSize, crmUserManager.getAll().size());
    }

    @Test
    public void testStudentManager() {
        EntityManager<Student> studentManager = new StudentManager();
        EntityManager<CrmUser> crmUserManager = new CrmUserManager();

        final int initialSize = studentManager.getAll().size();

        crmUserManager.insert(crmUser);
        studentManager.insert(student);

        Set<Student> students = studentManager.getAll();
        assertEquals(initialSize + 1, students.size());

        Student actualStudent = students.stream()
                .filter(s -> s.getId() == student.getId())
                .findFirst().get();

        assertEquals(student.isDeleted(), actualStudent.isDeleted());
        assertEquals(student.getCrmUser().getId(), actualStudent.getCrmUser().getId());

        studentManager.delete(actualStudent);
        crmUserManager.delete(crmUser);

        assertEquals(initialSize, studentManager.getAll().size());
    }

    @Test
    public void testCriterionManager() {
        EntityManager<Criterion> criterionManager = new CriterionManager();

        final int initialSize = criterionManager.getAll().size();

        criterionManager.insert(criterion);

        Set<Criterion> criterionSet = criterionManager.getAll();
        assertEquals(initialSize + 1, criterionSet.size());

        Criterion actual = criterionSet.stream()
                .filter(c -> criterion.getId() == c.getId())
                .findFirst().get();

        assertNotNull(actual);
        assertEquals(criterion.getMaxPoints(), actual.getMaxPoints());
        assertEquals(criterion.getTitle(), actual.getTitle());
        assertEquals(criterion.isAdditional(), actual.isAdditional());

        criterionManager.delete(actual);
        assertEquals(initialSize, criterionManager.getAll().size());
    }

    @Test
    public void testTaskManager() {
        EntityManager<Task> taskManager = new TaskManager();

        final int initialSize = taskManager.getAll().size();

        taskManager.insert(task);

        Set<Task> taskSet = taskManager.getAll();
        assertEquals(initialSize + 1, taskSet.size());

        Task actualTask = taskSet.stream()
                .filter(t -> task.getId() == t.getId())
                .findFirst().get();

        assertNotNull(actualTask);
        assertEquals(task.getDescription(), actualTask.getDescription());
        assertEquals(task.getTitle(), actualTask.getTitle());
        assertEquals(task.isLab(), actualTask.isLab());

        taskManager.delete(actualTask);

        assertEquals(initialSize, taskManager.getAll().size());
    }

    @Test
    public void testMarkManager() throws SQLException {
        EntityManager<CrmUser> crmUserManager = new CrmUserManager();
        EntityManager<Student> studentManager = new StudentManager();
        EntityManager<Criterion> criterionManager = new CriterionManager();
        EntityManager<Task> taskManager = new TaskManager();
        EntityManager<Mark> markManager = new MarkManager();

        final int initialSize = markManager.getAll().size();

        crmUserManager.insert(mark.getStudent().getCrmUser());
        studentManager.insert(mark.getStudent());
        criterionManager.insert(mark.getCriterion());
        taskManager.insert(mark.getTask());
        markManager.insert(mark);

        Set<Mark> markSet = markManager.getAll();
        assertEquals(initialSize + 1, markSet.size());

        Mark actualMark = markManager.getAll().stream()
                .filter(m -> mark.getId() == m.getId())
                .findFirst().get();

        assertNotNull(actualMark);
        assertEquals(mark.getId(), actualMark.getId());
        assertEquals(mark.getPoints(), actualMark.getPoints());
        assertEquals(mark.isDeleted(), actualMark.isDeleted());
        assertEquals(mark.getStudent().getId(), actualMark.getStudent().getId());
        assertEquals(mark.getCriterion().getId(), actualMark.getCriterion().getId());
        assertEquals(mark.getTask().getId(), actualMark.getTask().getId());

        try (PreparedStatement deleteConnection = ConnectionFactory.getInstance()
                .newConnection()
                .prepareStatement("DELETE FROM criterion_to_task " +
                        "WHERE criterion_id=? AND task_id=?")) {

            deleteConnection.setLong(1, actualMark.getCriterion().getId());
            deleteConnection.setLong(2, actualMark.getTask().getId());
            deleteConnection.executeUpdate();
        }

        markManager.delete(actualMark);
        taskManager.delete(actualMark.getTask());
        criterionManager.delete(actualMark.getCriterion());
        studentManager.delete(actualMark.getStudent());
        crmUserManager.delete(actualMark.getStudent().getCrmUser());

        assertEquals(initialSize, markManager.getAll().size());
    }
}