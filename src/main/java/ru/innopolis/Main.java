package ru.innopolis;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ru.innopolis.model.DatabaseManager;

/**
 *
 */
public class Main {

    static {
        PropertyConfigurator.configure(Main.class.getClassLoader()
                .getResource("log4j.properties"));
    }

    private static Logger LOGGER = Logger.getLogger(Main.class);

    /**
     *
     * @param args аргументы для запуска должны быть следующие:
     *             <code>-u<code/> для выгрузки из базы в xml
     *             или <code>-d</code> для загрузки из xml в базу
     */
    public static void main(String[] args) {

        if (args.length != 1) {
            LOGGER.error("Incorrect program arguments length!");
        } else {
            DatabaseManager databaseManager = new DatabaseManager();

            switch (args[0]) {
                case "-u":
                    databaseManager.loadAllToXml();
                    break;
                case "-d":
                    databaseManager.loadAllFromXml();
                    break;
                default:
                    LOGGER.error("Unknown program argument: " + args[0]);
                    break;
            }
        }
    }
}
