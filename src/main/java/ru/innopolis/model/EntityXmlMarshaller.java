package ru.innopolis.model;

import org.apache.log4j.Logger;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


/**
 * Класс осуществляет маршаллинг / анмаршаллинг xml
 * @param <T> тип сущности
 */
public class EntityXmlMarshaller<T> {

    private static final Logger LOGGER = Logger.getLogger(EntityXmlMarshaller.class);

    private Class<T> type;

    /**
     * Создает новый экземпляр класса {@link EntityXmlMarshaller}
     * @param type тип сущности, объект class класса, сгенерированного XJC
     */
    public EntityXmlMarshaller(Class<T> type) {
        this.type = type;
    }

    /**
     * Метод осуществляет маршаллинг сущности в xml
     * @param entity экземпляр класса, сгенерированного XJC
     * @param path путь к файлу, куда осуществлять маршаллинг
     */
    public void marshallToFile(T entity, String path) {

        try {
            File file = new File(path);
            JAXBContext jaxbContext = JAXBContext.newInstance(type);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(entity, file);

        } catch (JAXBException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Осуществляет анмаршаллинг сущности из xml-файла
     * @param path путь к файлу xml
     *
     * @return новый экземпляр сущности T или null в случае ошибки
     */
    @SuppressWarnings("unchecked")
    public T unmarshallFromFile(String path) {
        try {
            File file = new File(path);
            JAXBContext jaxbContext = JAXBContext.newInstance(type);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            return (T) jaxbUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
