package ru.innopolis.model.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ru.innopolis.model.xjc.CrmUser;
import ru.innopolis.model.xjc.Student;

/**
 * Реализация {@link EntityManager} для сущности {@link Student}
 */
public class StudentManager extends EntityManager<Student> {

    private static final String SELECT_ALL = "SELECT student.id AS student_id, " +
            "student.is_deleted AS student_is_deleted, student.crm_user_id AS crm_user_id FROM student";

    private static final String INSERT_STUDENT = "INSERT INTO student (id, is_deleted, crm_user_id) " +
            "VALUES (?, ?, ?)";

    public static final String DELETE_STUDENT_BY_ID = "DELETE FROM student WHERE id=?";

    @Override
    protected void fillInsertStatement(PreparedStatement insertStatement, Student entity) throws SQLException {
        insertStatement.setLong(1, entity.getId());
        insertStatement.setBoolean(2, entity.isDeleted());
        insertStatement.setLong(3, entity.getCrmUser().getId());
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement deleteStatement, Student entity) throws SQLException {
        deleteStatement.setLong(1, entity.getId());
    }

    @Override
    protected String getDeleteByIdStatement() {
        return DELETE_STUDENT_BY_ID;
    }

    @Override
    protected String getInsertSql() {
        return INSERT_STUDENT;
    }

    @Override
    protected String getSelectAllQuery() {
        return SELECT_ALL;
    }

    @Override
    protected String getSelectByPkQuery() {
        return SELECT_ALL + " WHERE id=?";
    }

    @Override
    protected Student createNewInstance(ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(resultSet.getLong("student_id"));
        student.setDeleted(resultSet.getBoolean("student_is_deleted"));

        CrmUser crmUser = new CrmUser();
        crmUser.setId(resultSet.getLong("crm_user_id"));
        student.setCrmUser(crmUser);

        return student;
    }
}
