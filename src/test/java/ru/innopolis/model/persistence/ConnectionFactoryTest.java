package ru.innopolis.model.persistence;

import org.junit.Test;

import java.sql.Connection;

import static org.junit.Assert.*;

/**
 *
 */
public class ConnectionFactoryTest {
    @Test
    public void newConnection() throws Exception {

        try(Connection connection = ConnectionFactory.getInstance().newConnection()) {
            assertNotNull(connection);
            assertEquals("PostgreSQL", connection.getMetaData().getDatabaseProductName());
        }
    }

}