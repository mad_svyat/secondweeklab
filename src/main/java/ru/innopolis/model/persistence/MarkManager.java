package ru.innopolis.model.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import ru.innopolis.model.xjc.Criterion;
import ru.innopolis.model.xjc.CrmUser;
import ru.innopolis.model.xjc.Mark;
import ru.innopolis.model.xjc.Student;
import ru.innopolis.model.xjc.Task;

/**
 * Реализация {@link EntityManager} для сущности {@link Mark}
 */
public class MarkManager extends EntityManager<Mark> {

    private static final String SELECT_ALL = "SELECT mark.id AS mark_id, mark.is_deleted AS mark_is_deleted, " +
            "mark.points AS mark_points, task.id AS task_id, task.title AS task_title, " +
            "task.description AS task_description, task.is_lab AS task_is_lab, task.created AS task_created, " +
            "criterion.id AS criterion_id, criterion.title AS criterion_title, criterion.max_points AS criterion_max_points, " +
            "criterion.is_additional AS criterion_is_additional, criterion.created AS criterion_created, " +
            "student.id AS student_id, student.is_deleted AS student_is_deleted, student.crm_user_id AS crm_user_id, crm_user.is_deleted AS crm_user_is_deleted, " +
            "crm_user.login AS crm_user_login, crm_user.user_password AS crm_user_password, " +
            "crm_user.first_name AS crm_user_first_name, crm_user.last_name AS crm_user_last_name, " +
            "crm_user.email AS crm_user_email, crm_user.is_active AS crm_user_is_active, " +
            "crm_user.is_deleted AS crm_user_is_deleted " +
            "FROM mark mark JOIN student student ON mark.student_id = student.id JOIN crm_user crm_user " +
            "ON student.crm_user_id = crm_user.id JOIN task task ON mark.task_id = task.id " +
            "JOIN criterion criterion ON mark.criterion_id = criterion.id";

    private static final String INSERT_CRITERION_TO_TASK = "INSERT INTO criterion_to_task " +
            "(task_id, criterion_id) VALUES(?, ?)";

    private static final String INSERT_MARK = "INSERT INTO mark (id, is_deleted, student_id, task_id, " +
            "criterion_id, points) VALUES (?, ?, ?, ?, ?, ?)";

    private static final String DELETE_MARK_BY_ID = "DELETE FROM mark WHERE id=?";

    @Override
    protected void fillInsertStatement(PreparedStatement insertStatement, Mark entity) throws SQLException {
        insertStatement.setLong(1, entity.getId());
        insertStatement.setBoolean(2, entity.isDeleted());
        insertStatement.setLong(3, entity.getStudent().getId());
        insertStatement.setLong(4, entity.getTask().getId());
        insertStatement.setLong(5, entity.getCriterion().getId());
        insertStatement.setInt(6, entity.getPoints());
    }

    @Override
    protected String getInsertSql() {
        return INSERT_MARK;
    }

    @Override
    public long insert(Mark entity) {

        long insertedMarkId = super.insert(entity);

        if (insertedMarkId > 0) {
            try (PreparedStatement insertTaskCriterionStatement = createPreparedStatement(INSERT_CRITERION_TO_TASK,
                    Statement.KEEP_CURRENT_RESULT)) {
                insertTaskCriterionStatement.setLong(1, entity.getTask().getId());
                insertTaskCriterionStatement.setLong(2, entity.getCriterion().getId());
                insertTaskCriterionStatement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return insertedMarkId;
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement deleteStatement, Mark entity) throws SQLException {
        deleteStatement.setLong(1, entity.getId());
    }

    @Override
    protected String getDeleteByIdStatement() {
        return DELETE_MARK_BY_ID;
    }

    @Override
    protected String getSelectAllQuery() {
        return SELECT_ALL;
    }

    @Override
    protected String getSelectByPkQuery() {
        return SELECT_ALL + " WHERE mark.id = ?";
    }

    @Override
    protected Mark createNewInstance(ResultSet resultSet) throws SQLException {
        Mark mark = new Mark();
        mark.setId(resultSet.getLong("mark_id"));
        mark.setPoints(resultSet.getInt("mark_points"));
        mark.setDeleted(resultSet.getBoolean("mark_is_deleted"));

        Task task = new Task();
        task.setId(resultSet.getLong("task_id"));
        task.setTitle(resultSet.getString("task_title"));
        task.setDescription(resultSet.getString("task_description"));
        task.setLab(resultSet.getBoolean("task_is_lab"));
        Calendar taskCalendar = Calendar.getInstance();
        resultSet.getDate("task_created", taskCalendar);
        task.setCreated(taskCalendar);

        mark.setTask(task);

        Criterion criterion = new Criterion();
        criterion.setId(resultSet.getLong("criterion_id"));
        criterion.setAdditional(resultSet.getBoolean("criterion_is_additional"));
        criterion.setMaxPoints(resultSet.getInt("criterion_max_points"));
        criterion.setTitle(resultSet.getString("criterion_title"));
        Calendar criterionCalendar = Calendar.getInstance();
        resultSet.getDate("criterion_created", criterionCalendar);
        criterion.setCreated(criterionCalendar);

        mark.setCriterion(criterion);

        CrmUser crmUser = new CrmUser();
        crmUser.setId(resultSet.getLong("crm_user_id"));
        crmUser.setLogin(resultSet.getString("crm_user_login"));
        crmUser.setEmail(resultSet.getString("crm_user_email"));
        crmUser.setFirstName(resultSet.getString("crm_user_first_name"));
        crmUser.setLastName(resultSet.getString("crm_user_last_name"));
        crmUser.setPassword(resultSet.getString("crm_user_password"));
        crmUser.setActive(resultSet.getBoolean("crm_user_is_active"));
        crmUser.setDeleted(resultSet.getBoolean("crm_user_is_deleted"));

        Student student = new Student();
        student.setId(resultSet.getLong("student_id"));
        student.setDeleted(resultSet.getBoolean("student_is_deleted"));
        student.setCrmUser(crmUser);

        mark.setStudent(student);

        return mark;
    }
}
