package ru.innopolis.model.persistence;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Класс содержит фабричный метод для создания соединений
 */
public class ConnectionFactory {

    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class);

    private static final ConnectionFactory INSTANCE = new ConnectionFactory();

    private Properties dbProperties;

    /**
     * Метод для получения единственного экземпляра класса
     * @return возвращает один и тот же экземпляр {@link ConnectionFactory}
     */
    public static ConnectionFactory getInstance() {
        return INSTANCE;
    }

    private ConnectionFactory() {
        dbProperties = new Properties();
        try (InputStream is = ConnectionFactory.class.getClassLoader()
                .getResourceAsStream("database.properties")) {
            dbProperties.load(is);

            Class.forName("org.postgresql.Driver");
        } catch (IOException | ClassNotFoundException  e) {
            LOGGER.error(e);
        }
    }

    /**
     * Создает новое соединение
     * @return новый экземпляр {@link Connection} или null в случае ошибки
     */
    public Connection newConnection() {

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(dbProperties.getProperty("url"),
                    dbProperties.getProperty("user"), dbProperties.getProperty("password"));
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return connection;
    }
}
