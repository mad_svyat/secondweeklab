package ru.innopolis.model.persistence;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import ru.innopolis.model.xjc.Criterion;

/**
 * Реализация {@link EntityManager} для сущности {@link Criterion}
 */
public class CriterionManager extends EntityManager<Criterion> {

    private static final String SELECT_ALL = "SELECT criterion.id AS criterion_id, criterion.title AS criterion_title, " +
            "criterion.max_points AS criterion_max_points, criterion.is_additional AS criterion_is_additional, " +
            "criterion.created AS criterion_created FROM criterion criterion";

    private static final String INSERT_CRITERION = "INSERT INTO criterion (id, title, max_points, " +
            "is_additional, created) VALUES(?, ?, ?, ?, ?)";

    private static final String DELETE_CRITERION_BY_ID = "DELETE FROM criterion WHERE id=?";

    @Override
    protected void fillInsertStatement(PreparedStatement insertStatement, Criterion entity) throws SQLException {
        insertStatement.setLong(1, entity.getId());
        insertStatement.setString(2, entity.getTitle());
        insertStatement.setInt(3, entity.getMaxPoints());
        insertStatement.setBoolean(4, entity.isAdditional());
        insertStatement.setDate(5, new Date(entity.getCreated().getTimeInMillis()));
    }

    @Override
    protected void fillDeleteStatement(PreparedStatement deleteStatement, Criterion entity) throws SQLException {
        deleteStatement.setLong(1,entity.getId());
    }

    @Override
    protected String getDeleteByIdStatement() {
        return DELETE_CRITERION_BY_ID;
    }

    @Override
    protected String getInsertSql() {
        return INSERT_CRITERION;
    }

    @Override
    protected String getSelectAllQuery() {
        return SELECT_ALL;
    }

    @Override
    protected String getSelectByPkQuery() {
        return SELECT_ALL + " WHERE id=?";
    }

    @Override
    protected Criterion createNewInstance(ResultSet resultSet) throws SQLException {
        Criterion criterion = new Criterion();
        criterion.setId(resultSet.getLong("criterion_id"));
        criterion.setAdditional(resultSet.getBoolean("criterion_is_additional"));
        criterion.setMaxPoints(resultSet.getInt("criterion_max_points"));
        criterion.setTitle(resultSet.getString("criterion_title"));
        Calendar criterionCalendar = Calendar.getInstance();
        resultSet.getTimestamp("criterion_created", criterionCalendar);
        criterion.setCreated(criterionCalendar);

        return criterion;
    }
}
