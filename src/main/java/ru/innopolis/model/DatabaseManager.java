package ru.innopolis.model;

import ru.innopolis.model.persistence.CriterionManager;
import ru.innopolis.model.persistence.CrmUserManager;
import ru.innopolis.model.persistence.EntityManager;
import ru.innopolis.model.persistence.MarkManager;
import ru.innopolis.model.persistence.StudentManager;
import ru.innopolis.model.persistence.TaskManager;
import ru.innopolis.model.xjc.Criterion;
import ru.innopolis.model.xjc.Criterions;
import ru.innopolis.model.xjc.CrmUser;
import ru.innopolis.model.xjc.CrmUsers;
import ru.innopolis.model.xjc.Mark;
import ru.innopolis.model.xjc.Marks;
import ru.innopolis.model.xjc.Student;
import ru.innopolis.model.xjc.Students;
import ru.innopolis.model.xjc.Task;
import ru.innopolis.model.xjc.Tasks;

/**
 * Класс отвечает за выгрузку из базы в xml и из xml в базу
 */
public class DatabaseManager {

    private static final String CRM_USERS_XML = "crm_users.xml";
    private static final String STUDENTS_XML = "students.xml";
    private static final String CRITERIONS_XML = "criterions.xml";
    private static final String MARKS_XML = "marks.xml";
    private static final String TASKS_XML = "tasks.xml";

    private EntityManager<CrmUser> crmUserManager = new CrmUserManager();
    private EntityManager<Student> studentManager = new StudentManager();
    private EntityManager<Task> taskManager = new TaskManager();
    private EntityManager<Criterion> criterionManager = new CriterionManager();
    private EntityManager<Mark> markManager = new MarkManager();

    /**
     * Метод выгружает данные о всех сущностях из базы в xml-файлы
     */
    public void loadAllToXml() {

        CrmUsers crmUsers = new CrmUsers();
        crmUsers.getCrmUser().addAll(crmUserManager.getAll());
        EntityXmlMarshaller<CrmUsers> crmUserXmlMarshaller = new EntityXmlMarshaller<>(CrmUsers.class);
        crmUserXmlMarshaller.marshallToFile(crmUsers, CRM_USERS_XML);

        Students students = new Students();
        students.getStudent().addAll(studentManager.getAll());
        EntityXmlMarshaller<Students> studentsXmlMarshaller = new EntityXmlMarshaller<>(Students.class);
        studentsXmlMarshaller.marshallToFile(students, STUDENTS_XML);

        Tasks tasks = new Tasks();
        tasks.getTask().addAll(taskManager.getAll());
        EntityXmlMarshaller<Tasks> tasksXmlMarshaller = new EntityXmlMarshaller<>(Tasks.class);
        tasksXmlMarshaller.marshallToFile(tasks, TASKS_XML);

        Criterions criterions = new Criterions();
        criterions.getCriterion().addAll(criterionManager.getAll());
        EntityXmlMarshaller<Criterions> criterionsXmlMarshaller = new EntityXmlMarshaller<>(Criterions.class);
        criterionsXmlMarshaller.marshallToFile(criterions, CRITERIONS_XML);

        Marks marks = new Marks();
        marks.getMark().addAll(markManager.getAll());
        EntityXmlMarshaller<Marks> marksXmlMarshaller = new EntityXmlMarshaller<>(Marks.class);
        marksXmlMarshaller.marshallToFile(marks, MARKS_XML);
    }

    /**
     * метод выгружает данные из xml-файлов в базу
     */
    public void loadAllFromXml() {

        EntityXmlMarshaller<CrmUsers> crmUsersXmlMarshaller = new EntityXmlMarshaller<>(CrmUsers.class);
        CrmUsers crmUsers = crmUsersXmlMarshaller.unmarshallFromFile(CRM_USERS_XML);
        for (CrmUser crmUser : crmUsers.getCrmUser()) {
            crmUserManager.insert(crmUser);
        }

        EntityXmlMarshaller<Students> studentsXmlMarshaller = new EntityXmlMarshaller<>(Students.class);
        Students students = studentsXmlMarshaller.unmarshallFromFile(STUDENTS_XML);
        for (Student student : students.getStudent()) {
            studentManager.insert(student);
        }

        EntityXmlMarshaller<Tasks> tasksXmlMarshaller = new EntityXmlMarshaller<>(Tasks.class);
        Tasks tasks = tasksXmlMarshaller.unmarshallFromFile(TASKS_XML);
        for (Task task : tasks.getTask()) {
            taskManager.insert(task);
        }

        EntityXmlMarshaller<Criterions> criterionsXmlMarshaller = new EntityXmlMarshaller<>(Criterions.class);
        Criterions criterions = criterionsXmlMarshaller.unmarshallFromFile(CRITERIONS_XML);
        for (Criterion criterion : criterions.getCriterion()) {
            criterionManager.insert(criterion);
        }

        EntityXmlMarshaller<Marks> marksEntityXmlMarshaller = new EntityXmlMarshaller<>(Marks.class);
        Marks marks = marksEntityXmlMarshaller.unmarshallFromFile(MARKS_XML);
        for (Mark mark : marks.getMark()) {
            markManager.insert(mark);
        }
    }
}
