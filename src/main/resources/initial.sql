INSERT INTO task VALUES (1, 'Лабораторная 1', 'Необходимо разработать программу, которая получает на вход список ресурсов,
 содержащих текст, и считает общее количество вхождений (для всех ресурсов)
 каждого слова. Каждый ресурс должен быть обработан в отдельном потоке,
 текст не должен содержать инностранных символов, только кириллица,
 знаки препинания и цифры. Отчет должен строиться в режиме реального времени, знаки препинания и цифры в отчет не входят.
 Все ошибки должны быть корректно обработаны, все API покрыто модульными тестами', true, NULL);

INSERT INTO criterion VALUES (1, 'Работоспособность кода', 40, false, NULL);
INSERT INTO criterion VALUES (2, 'Соблюдение сроков сдачи', 10, false, NULL);
INSERT INTO criterion VALUES (3, 'Наличие javadoc', 10, false, NULL);
INSERT INTO criterion VALUES (4, 'Использование ООП', 10, false, NULL);
INSERT INTO criterion VALUES (5, 'Отслеживание остановки потока', 10, false, NULL);
INSERT INTO criterion VALUES (6, 'Отсутствие data racing', 10, false, NULL);
INSERT INTO criterion VALUES (7, 'Корректность по модели памяти', 10, false, NULL);


INSERT INTO criterion_to_task VALUES (1, 1, 1);
INSERT INTO criterion_to_task VALUES (2, 1, 2);
INSERT INTO criterion_to_task VALUES (3, 1, 3);
INSERT INTO criterion_to_task VALUES (4, 1, 4);
INSERT INTO criterion_to_task VALUES (5, 1, 5);
INSERT INTO criterion_to_task VALUES (6, 1, 6);
INSERT INTO criterion_to_task VALUES (7, 1, 7);

INSERT INTO crm_user VALUES (4, 'cheg', 'cb42e130d1471239a27fca6228094f0e', 'Черкасов', 'Гена', 'gena@cherkasov.com', true, false);
INSERT INTO crm_user VALUES (5, 'billyboy', '8d2e88e1afeed3c4a845a61abe556c5d', 'Bill', 'Gates', 'bill@microsoft.com', true, false);
INSERT INTO crm_user VALUES (6, 'jhon', 'bd2b5884c8580e6f95c25e5042e9ce06', 'Johnnie', 'Walker', 'walker@gmail.com', true, false);

INSERT INTO student VALUES (4, false, 4);
INSERT INTO student VALUES (5, false, 5);
INSERT INTO student VALUES (6, false, 6);

INSERT INTO mark VALUES (1, false, 4, 1, 1, 35);
INSERT INTO mark VALUES (2, false, 4, 1, 2, 8);
INSERT INTO mark VALUES (3, false, 4, 1, 3, 8);
INSERT INTO mark VALUES (4, false, 4, 1, 4, 7);
INSERT INTO mark VALUES (5, false, 4, 1, 5, 6);
INSERT INTO mark VALUES (6, false, 4, 1, 6, 9);
INSERT INTO mark VALUES (7, false, 4, 1, 7, 3);

