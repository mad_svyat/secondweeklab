package ru.innopolis.model.persistence;

import org.junit.Test;

import ru.innopolis.model.EntityXmlMarshaller;
import ru.innopolis.model.xjc.CrmUser;
import ru.innopolis.model.xjc.CrmUsers;

import static org.junit.Assert.*;

/**
 *
 */
public class EntityXmlMarshallerTest {
    @Test
    public void marshallToFile() throws Exception {

        CrmUsers expectedUsers = new CrmUsers();

        for (int i = 0; i < 10; i++) {
            CrmUser crmUser = new CrmUser();
            crmUser.setId(i);
            crmUser.setDeleted(false);
            crmUser.setActive(true);
            crmUser.setPassword("*" + i + "#");
            crmUser.setFirstName("first: " + i);
            crmUser.setLastName("last: " + i);
            crmUser.setLogin("login#" + i);
            crmUser.setEmail("mail" + i + "@gmail.com");
            expectedUsers.getCrmUser().add(crmUser);
        }

        EntityXmlMarshaller<CrmUsers> crmUserMarshaller = new EntityXmlMarshaller<>(CrmUsers.class);
        crmUserMarshaller.marshallToFile(expectedUsers, "crm_test.xml");

        CrmUsers unmarshalledUsers = crmUserMarshaller.unmarshallFromFile("crm_test.xml");

        assertEquals(expectedUsers.getCrmUser().size(), unmarshalledUsers.getCrmUser().size());

        CrmUser expected;
        CrmUser actual;
        for (int i = 0; i < expectedUsers.getCrmUser().size(); i++) {
            expected = expectedUsers.getCrmUser().get(i);
            actual = unmarshalledUsers.getCrmUser().get(i);
            assertEquals(expected.getLogin(), actual.getLogin());
            assertEquals(expected.getEmail(), actual.getEmail());
            assertEquals(expected.getFirstName(), actual.getFirstName());
            assertEquals(expected.getId(), actual.getId());
            assertEquals(expected.isActive(), actual.isActive());
            assertEquals(expected.isDeleted(), actual.isDeleted());
            assertEquals(expected.getPassword(), actual.getPassword());
            assertEquals(expected.getLastName(), actual.getLastName());
        }
    }
}